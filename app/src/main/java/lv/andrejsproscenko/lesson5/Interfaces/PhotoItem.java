package lv.andrejsproscenko.lesson5.Interfaces;

public interface PhotoItem {
    String getImgUrl();
    String getAuthorName();
}
