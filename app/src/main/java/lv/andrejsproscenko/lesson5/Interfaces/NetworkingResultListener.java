package lv.andrejsproscenko.lesson5.Interfaces;

public interface NetworkingResultListener {
    void callback(PhotoItem[] photoItems);
}
