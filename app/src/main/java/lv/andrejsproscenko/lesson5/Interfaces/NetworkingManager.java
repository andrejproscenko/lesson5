package lv.andrejsproscenko.lesson5.Interfaces;

public interface NetworkingManager {
    void getPhotoItems(NetworkingResultListener result);
}
