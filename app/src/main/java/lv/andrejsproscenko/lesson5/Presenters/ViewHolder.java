package lv.andrejsproscenko.lesson5.Presenters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import lv.andrejsproscenko.lesson5.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewHolder {
    @BindView(R.id.imageView) ImageView imageViewPhoto;
    @BindView(R.id.textViewAuthor) TextView textViewAuthor;

    public ViewHolder(View view) {
        ButterKnife.bind(this, view);
    }
}
