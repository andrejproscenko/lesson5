package lv.andrejsproscenko.lesson5.DataSources.Unsplash;

import lv.andrejsproscenko.lesson5.Interfaces.PhotoItem;

import java.util.Map;

public class PhotoItemUnsplash implements PhotoItem {

    Map<String,String> urls;
    User user;

    public String getImgUrl() {
        return this.urls.get("regular");
    }

    public String getAuthorName() {
        return this.user.name;
    }

    public class User {
        String name;
    }
}
