package lv.andrejsproscenko.lesson5.DataSources.Giphy;

import lv.andrejsproscenko.lesson5.Interfaces.PhotoItem;

import java.util.Map;

public class PhotoItemGiphy implements PhotoItem {

    Map<String,Map<String, String>> images;
    User user;

    public String getImgUrl() {
        return this.images.get("downsized_medium").get("url");
    }

    public String getAuthorName() {
        return user != null ? this.user.username : "";
    }

    public class User {
        String username;
    }
}
